/* docview.c 
 * viewer for bonobo compound files 
 * similar to the DocViewer tool MS ships with Visual Studio
 */
   
/*
** Copyright (C) 1999 Dirk-Jan C. Binnema <djcb@dds.nl>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <orb/orbit.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-bonobo.h>
#include <bonobo/gnome-object.h>
#include <bonobo/gnome-stream.h>
#include <bonobo/gnome-storage.h>

/* declarations */
void cb_open (GtkWidget *widget, gpointer data);
void cb_close(GtkWidget *widget, gpointer data);
void cb_exit (GtkWidget *widget,  gpointer data);
void cb_about (GtkWidget *widget, gpointer data);
void cb_open_ok (GtkFileSelection* file_selection, gpointer data);
gboolean fill_tree(GtkCTreeNode *node, GnomeStorage *storage);
gchar* file_from_path (gchar *path);
gint build_gui(); 

/* menus */
static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM(("_Open file"),
                            ("Open a Bonobo compound document file"), 
                            cb_open, NULL),
	GNOMEUIINFO_MENU_CLOSE_ITEM(cb_close,NULL),
	GNOMEUIINFO_MENU_EXIT_ITEM(cb_exit,NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM(("_About..."),
			    ("About Bonobo Docviewer"),
			    cb_about, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};


CORBA_Environment ev;
CORBA_ORB orb;
GnomeStorage *storage;
GtkWidget *ctree, *msgbox;
GtkCTreeNode *file_node;
GdkPixmap *pixopen, *pixclose;
GdkBitmap *bitopen, *bitclose;


gint
build_gui() 
{
	GtkWidget *app;
	
	app= gnome_app_new ("docview", "Bonobo Doc Viewer");
	
	gnome_app_create_menus (GNOME_APP(app), main_menu);
      
	gtk_widget_set_usize (GTK_WIDGET(app), 300,200);
	
	gtk_signal_connect_object(GTK_OBJECT(GTK_WINDOW(app)), "destroy",
				  cb_exit, NULL);

	ctree= gtk_ctree_new (3,2);
	gnome_app_set_contents((GNOME_APP(app)), ctree);
       
	gtk_widget_show_all(GTK_WIDGET(app));

	return TRUE;
}



/* recursively fill tree */
gboolean
fill_tree(GtkCTreeNode *node, GnomeStorage *storage)
{
	CORBA_char* path="/";
	CORBA_Environment ev;
	CORBA_Object obj;
	GNOME_Storage_directory_list *dir;
	GnomeStorage *my_storage;
	GtkCTreeNode *sibling;
	gboolean is_leaf;

	gchar* names[3]={NULL,NULL,NULL};
	int i;

	obj= GNOME_OBJECT(storage)->corba_objref;
	dir= GNOME_Storage_list_contents (obj, path, &ev);

	if ( CORBA_NO_EXCEPTION == ev._major ) {
		for (i=0; i < dir->_length; i++) {
			names[2]=strdup(dir->_buffer[i]);

			/* test if it's a stream (leaf) or storage */
			my_storage= gnome_storage_open ("efs", dir->_buffer[i], GNOME_SS_READ, 0644);
			g_print ("trying %s...", dir->_buffer[i]);
			is_leaf = ( NULL == my_storage );
			g_print ("it's a %s", is_leaf?"file\n":"dir\n");

			sibling= gtk_ctree_insert_node(GTK_CTREE(ctree), file_node, NULL,
						       names, 0, NULL, NULL, NULL, NULL, is_leaf, TRUE);
 
			if (! is_leaf ) {
				fill_tree ( sibling, my_storage);
			}

			if (my_storage)
				gnome_object_unref(GNOME_OBJECT(my_storage));
			
		}
	} else {
		g_print (_("Error getting dir\n"));
	}
	CORBA_free(dir);
       
	return TRUE;
}


void
cb_open ( GtkWidget *widget, gpointer data)
{
	GtkWidget *filedialog;
       
	filedialog  =  gtk_file_selection_new(_("Open Bonobo Compound File"));
	

	gtk_signal_connect_object (GTK_OBJECT(GTK_FILE_SELECTION(filedialog)->ok_button),
					      "clicked", GTK_SIGNAL_FUNC (cb_open_ok),
					      (gpointer)filedialog);
	gtk_signal_connect_object (GTK_OBJECT(GTK_FILE_SELECTION(filedialog)->ok_button),
					      "clicked", GTK_SIGNAL_FUNC (gtk_widget_destroy),
					      (gpointer)filedialog);
	gtk_signal_connect_object (GTK_OBJECT(GTK_FILE_SELECTION(filedialog)->cancel_button),
					      "clicked", GTK_SIGNAL_FUNC (gtk_widget_destroy),
					      (gpointer)filedialog);
       
        gtk_widget_show(GTK_WIDGET(filedialog));
}


void
cb_open_ok (GtkFileSelection* file_selection, gpointer data)
{
	gchar* filename= gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selection));
	gchar *names[]={file_from_path(filename), NULL};

	/* try to open the storage */
	storage= gnome_storage_open ("efs", filename, GNOME_SS_RDWR|GNOME_SS_CREATE, 0644);
	if ( NULL == storage ) {
		
		msgbox= gnome_message_box_new (_("Error opening EFS file"),
					       GNOME_MESSAGE_BOX_ERROR, _("Ok"), NULL);
		gnome_dialog_run (GNOME_DIALOG(msgbox));
		
		return;
	}
	g_print ("Opening storage succeeded\n");
	file_node= gtk_ctree_insert_node (GTK_CTREE(ctree), NULL, NULL, 
					  names, 0, NULL, NULL, NULL, NULL, FALSE, TRUE);
	
	fill_tree(file_node, storage);
	
}

void
cb_close ( GtkWidget *widget, gpointer data)
{
	if (storage)
	       gnome_object_unref(GNOME_OBJECT(storage));
}


void
cb_exit (GtkWidget *widget, gpointer data)
{
	/* close the storage first */
	cb_close (NULL, NULL);

	g_print (_("Thank you for using Bonobo Doc Viewer....\n"));

	CORBA_exception_free(&ev); 
	gtk_main_quit();
}


void
cb_about (GtkWidget *widget, gpointer data)
{
	GtkWidget *about;
	const gchar *authors[]= { N_("Dirk-Jan C. Binnema <djcb@dds.nl>"), NULL };
	about= gnome_about_new ("Bonobo Doc Viewer", "0.0.0",
				_("(c) 1999 Dirk-Jan C. Binnema"),
				authors,
				_("Bonobo Doc Viewer\nReleased under the terms of the GPL"), 
				NULL);
	gtk_widget_show (GTK_WIDGET(about));	
}


gchar*
file_from_path (gchar *path)
{
	int cursor;
	int last  = strlen(path) - 1;
	
	for ( cursor = last; cursor; cursor--) {
		
		if ( '/' == path[cursor] )
			break;
	} 
	
	return (( '/' == path[cursor] )? (&path[cursor + 1]): (path));
}



gint 
main (gint argc, gchar *argv[])
{

	CORBA_exception_init(&ev);
	gnome_CORBA_init ("docview", "0.0", &argc, argv, 0, &ev);
	orb= gnome_CORBA_ORB();

	if ( FALSE == bonobo_init(orb,0,0)) {
     		g_print (_("Error initializing Bonobo...exiting."));
		return -1;
	}

       build_gui();
       gtk_main();

       return 0;
}








