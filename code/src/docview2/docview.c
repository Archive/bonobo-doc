/* docview.c 
 * viewer for bonobo compound files 
 * similar to the DocViewer tool MS ships with Visual Studio
 */
   
/*
** Copyright (C) 1999 Mathieu Lacage <mathieu@advogato.org>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

#include <gnome.h>
#include <efs.h>

/* declarations */
void cb_create (GtkWidget *widget, gpointer data);
void cb_open (GtkWidget *widget, gpointer data);
void cb_close(GtkWidget *widget, gpointer data);
void cb_exit (GtkWidget *widget,  gpointer data);
void cb_about (GtkWidget *widget, gpointer data);
GtkWidget *build_gui (void); 
void build_ui (GtkWidget *root_tree, EFSDir *dir);


GtkWidget *
build_gui (void) 
{
  GtkWidget *app = gnome_app_new ("docview", "LibEFS Doc Viewer");
  /* menus */
  GnomeUIInfo file_menu[] = {
    GNOMEUIINFO_MENU_NEW_ITEM(("_New"),
			      ("Create a new compound file."), 
			      cb_create, NULL),
    GNOMEUIINFO_MENU_OPEN_ITEM(cb_open, app),
    GNOMEUIINFO_MENU_CLOSE_ITEM(cb_close,NULL),
    GNOMEUIINFO_MENU_EXIT_ITEM(cb_exit,NULL),
    GNOMEUIINFO_END
  };
  GnomeUIInfo help_menu[] = {
    GNOMEUIINFO_MENU_ABOUT_ITEM(cb_about, NULL),
    GNOMEUIINFO_END
  };

  GnomeUIInfo main_menu[] = {
    GNOMEUIINFO_MENU_FILE_TREE(file_menu),
    GNOMEUIINFO_MENU_HELP_TREE(help_menu),
    GNOMEUIINFO_END
  };


	
  gnome_app_create_menus (GNOME_APP(app), main_menu);
      
  gtk_widget_set_usize (GTK_WIDGET(app), 150,400);
	
  gtk_signal_connect_object(GTK_OBJECT(GTK_WINDOW(app)), "destroy",
			    cb_exit, NULL);

  gtk_widget_show_all(GTK_WIDGET(app));
  
  return app;
}



/* create a test efs file. */
void 
cb_create (GtkWidget *widget, gpointer data)
{
  EFSDir *root_dir = NULL;
  int i = 0, j = 0;


  root_dir = efs_open ("./test.efs", EFS_RDWR | EFS_CREATE, 0700); 
  if (root_dir == NULL) {
    g_warning ("Error: Could not create test.efs");
    cb_exit (NULL, NULL);
  }
  for (i=0; i<5; i++) {
    EFSDir *sub_dir = NULL;
    sub_dir = efs_dir_open (root_dir, g_strdup_printf ("subdir%d", i), EFS_RDWR | EFS_CREATE);
    if (sub_dir == NULL) {
      g_warning ("Error: Could not create subdir %d", i);
      cb_exit (NULL, NULL);
    }
    for (j=0; j<5; j++) {
      EFSDir *file = NULL;
      file = efs_file_open (sub_dir, g_strdup_printf ("file%d", j), EFS_RDWR | EFS_CREATE);
      if (file == NULL) {
	g_warning ("Error: Could not create file %d in subdir %d", j, i);
	cb_exit (NULL, NULL);
      }
      efs_file_close (file);
    }
    efs_dir_close (sub_dir);
  }

  efs_commit (root_dir);
  efs_dir_close (root_dir);

}


void
cb_open ( GtkWidget *widget, gpointer data)
{
  GtkWidget *tree = NULL, *app = GTK_WIDGET (data);
  EFSDir *dir = NULL;

  /* try to open the storage */
  
  dir = efs_open ("./test.efs", EFS_READ, 0);
  if (dir == NULL) {
    g_warning ("Error: Could not open test.efs\n");
    cb_exit (NULL, NULL);
  }

  tree = gtk_tree_new ();
  build_ui (tree, dir);

  g_print (g_strdup_printf ("%d",   efs_close (dir)));
  gnome_app_set_contents((GNOME_APP(app)), tree);
  gtk_widget_show_all (GTK_WIDGET(app));

}


/* create recursively the tree representing the filesystem */
void
build_ui (GtkWidget *root_tree, EFSDir *dir)
{
  EFSDirEntry *entry = NULL;
  GtkWidget *item = NULL, *tree = NULL;
  EFSDir *sub_dir = NULL;

  entry = efs_dir_read (dir);
  while (entry != NULL) {
    g_print (entry->name);
    g_print ("\n");
    item = gtk_tree_item_new_with_label (entry->name);
    gtk_widget_show (GTK_WIDGET(item));
    gtk_tree_append (GTK_TREE(root_tree), GTK_WIDGET(item));

    if (entry->type == EFS_DIR) {
      tree = gtk_tree_new ();
      gtk_tree_item_set_subtree (GTK_TREE_ITEM(item), GTK_WIDGET(tree));
      sub_dir = efs_dir_open (dir, entry->name, 0);
      build_ui (tree, sub_dir);
      gtk_widget_show (GTK_WIDGET(tree));
    } else if (entry->type == EFS_FILE) {
    }
    entry = efs_dir_read (dir);
  }

}


void
cb_close ( GtkWidget *widget, gpointer data)
{
}


void
cb_exit (GtkWidget *widget, gpointer data)
{
	/* close the storage first */
	cb_close (NULL, NULL);

	g_print (_("Thank you for using LibEFS Doc Viewer....\n"));

	gtk_main_quit();
	exit (1);
}


void
cb_about (GtkWidget *widget, gpointer data)
{
	GtkWidget *about;
	const gchar *authors[]= { N_("Mathieu Lacage <mathieu@advogato.org>"), NULL };
	about= gnome_about_new ("LibEFS Doc Viewer", "0.0.0",
				_("(c) 1999 Mathieu Lacage"),
				authors,
				_("LibEFS Doc Viewer\nReleased under the terms of the GPL"), 
				NULL);
	gtk_widget_show (GTK_WIDGET(about));
}



gint 
main (gint argc, gchar *argv[])
{
  GtkWidget *app;
  gnome_init ("DocView", "0.0", argc, argv);
  
  
  app = build_gui();
  gtk_main();
  
  return 0;
}








