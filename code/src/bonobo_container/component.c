#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <ORBitservices/CosNaming.h>


#include "bonobo.h"

/*** App-specific servant structures ***/

typedef struct
{
   POA_GNOME_Persist servant;
   PortableServer_POA poa;

}
impl_POA_GNOME_Persist;

typedef struct
{
  POA_GNOME_Embeddable servant;
  PortableServer_POA poa;
  
  GNOME_ClientSite client_site;
  gchar data[11];
  GSList *view_servant_list;
  gchar *host_name;
  gchar *uri;
}
impl_POA_GNOME_Embeddable;

typedef struct
{
  POA_GNOME_View servant;
  PortableServer_POA poa;

  impl_POA_GNOME_Embeddable *embeddable_servant;
  GNOME_ViewFrame frame;
  GtkWidget *widget;
  GtkWidget *plug;

}
impl_POA_GNOME_View;



typedef struct
{
   POA_GNOME_EmbeddableFactory servant;
   PortableServer_POA poa;
}
impl_POA_GNOME_EmbeddableFactory;

/*** Implementation stub prototypes ***/


static void impl_GNOME_View__destroy(impl_POA_GNOME_View * servant,
				     CORBA_Environment * ev);

static void
impl_GNOME_View_size_allocate(impl_POA_GNOME_View * servant,
			      CORBA_short width,
			      CORBA_short height, CORBA_Environment * ev);

static void
impl_GNOME_View_size_query(impl_POA_GNOME_View * servant,
			   CORBA_short * desired_width,
			   CORBA_short * desired_height,

			   CORBA_Environment * ev);

static void
impl_GNOME_View_set_window(impl_POA_GNOME_View * servant,
			   GNOME_View_windowid id, 
			   CORBA_Environment * ev);

static void
impl_GNOME_View_activate(impl_POA_GNOME_View * servant,
			 CORBA_boolean activated, 
			 CORBA_Environment * ev);

static void
impl_GNOME_View_reactivate_and_undo(impl_POA_GNOME_View * servant,
				    CORBA_Environment * ev);

static void
impl_GNOME_View_do_verb(impl_POA_GNOME_View * servant,
			CORBA_char * verb_name, 
			CORBA_Environment * ev);

static void
impl_GNOME_View_set_zoom_factor(impl_POA_GNOME_View * servant,
				CORBA_double zoom, 
				CORBA_Environment * ev);

static void
impl_GNOME_View_ref(impl_POA_GNOME_View * servant, 
		    CORBA_Environment * ev);
static void
impl_GNOME_View_unref(impl_POA_GNOME_View * servant, 
		      CORBA_Environment * ev);
static CORBA_Object
impl_GNOME_View_query_interface(impl_POA_GNOME_View * servant,
				CORBA_char * repoid, 
				CORBA_Environment * ev);

static void impl_GNOME_Embeddable__destroy(impl_POA_GNOME_Embeddable *
					   servant, 
					   CORBA_Environment * ev);
static void impl_GNOME_Embeddable_set_client_site(impl_POA_GNOME_Embeddable *
						  servant,
						  GNOME_ClientSite
						  client_site,
						  CORBA_Environment * ev);

static GNOME_ClientSite
impl_GNOME_Embeddable_get_client_site(impl_POA_GNOME_Embeddable * servant,
				      CORBA_Environment * ev);

static void
impl_GNOME_Embeddable_set_host_name(impl_POA_GNOME_Embeddable * servant,
				    CORBA_char * name,
				    CORBA_char * appname,
				    CORBA_Environment * ev);

static void
impl_GNOME_Embeddable_set_uri(impl_POA_GNOME_Embeddable * servant,
			      CORBA_char * uri, 
			      CORBA_Environment * ev);

static void
impl_GNOME_Embeddable_close(impl_POA_GNOME_Embeddable * servant,
			    GNOME_Embeddable_CloseMode mode,
			    CORBA_Environment * ev);

static GNOME_Embeddable_verb_list
   *impl_GNOME_Embeddable_get_verb_list(impl_POA_GNOME_Embeddable * servant,
					CORBA_Environment * ev);

static GNOME_View
impl_GNOME_Embeddable_new_view(impl_POA_GNOME_Embeddable * servant,
			       GNOME_ViewFrame frame, 
			       CORBA_Environment * ev);

static void
impl_GNOME_Embeddable_ref(impl_POA_GNOME_Embeddable * servant,
			  CORBA_Environment * ev);
static void
impl_GNOME_Embeddable_unref(impl_POA_GNOME_Embeddable * servant,
			    CORBA_Environment * ev);
static CORBA_Object
impl_GNOME_Embeddable_query_interface(impl_POA_GNOME_Embeddable * servant,
				      CORBA_char * repoid,
				      CORBA_Environment * ev);

static void
impl_GNOME_EmbeddableFactory__destroy(impl_POA_GNOME_EmbeddableFactory *
				      servant, 
				      CORBA_Environment * ev);
static CORBA_boolean
impl_GNOME_EmbeddableFactory_supports(impl_POA_GNOME_EmbeddableFactory *
				      servant, 
				      CORBA_char * obj_goad_id,
				      CORBA_Environment * ev);
static CORBA_Object
impl_GNOME_EmbeddableFactory_create_object(impl_POA_GNOME_EmbeddableFactory *
					   servant, 
					   CORBA_char * goad_id,
					   GNOME_stringlist * params,
					   CORBA_Environment * ev);

/*** epv structures ***/

static PortableServer_ServantBase__epv impl_GNOME_View_base_epv = {
   NULL,			/* _private data */
   NULL,			/* finalize routine */
   NULL,			/* default_POA routine */
};
static POA_GNOME_View__epv impl_GNOME_View_epv = {
   NULL,			/* _private */

   (gpointer) & impl_GNOME_View_size_allocate,

   (gpointer) & impl_GNOME_View_size_query,

   (gpointer) & impl_GNOME_View_set_window,

   (gpointer) & impl_GNOME_View_activate,

   (gpointer) & impl_GNOME_View_reactivate_and_undo,

   (gpointer) & impl_GNOME_View_do_verb,

   (gpointer) & impl_GNOME_View_set_zoom_factor,

};
static POA_GNOME_Unknown__epv impl_GNOME_View_GNOME_Unknown_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GNOME_View_ref,
   (gpointer) & impl_GNOME_View_unref,
   (gpointer) & impl_GNOME_View_query_interface,
};
static PortableServer_ServantBase__epv impl_GNOME_Embeddable_base_epv = {
   NULL,			/* _private data */
   NULL,			/* finalize routine */
   NULL,			/* default_POA routine */
};
static POA_GNOME_Embeddable__epv impl_GNOME_Embeddable_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GNOME_Embeddable_set_client_site,

   (gpointer) & impl_GNOME_Embeddable_get_client_site,

   (gpointer) & impl_GNOME_Embeddable_set_host_name,

   (gpointer) & impl_GNOME_Embeddable_set_uri,

   (gpointer) & impl_GNOME_Embeddable_close,

   (gpointer) & impl_GNOME_Embeddable_get_verb_list,

   (gpointer) & impl_GNOME_Embeddable_new_view,

};
static POA_GNOME_Unknown__epv impl_GNOME_Embeddable_GNOME_Unknown_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GNOME_Embeddable_ref,
   (gpointer) & impl_GNOME_Embeddable_unref,
   (gpointer) & impl_GNOME_Embeddable_query_interface,
};
static PortableServer_ServantBase__epv impl_GNOME_EmbeddableFactory_base_epv = {
   NULL,			/* _private data */
   NULL,			/* finalize routine */
   NULL,			/* default_POA routine */
};
static POA_GNOME_EmbeddableFactory__epv impl_GNOME_EmbeddableFactory_epv = {
   NULL,			/* _private */
};
static POA_GNOME_GenericFactory__epv
   impl_GNOME_EmbeddableFactory_GNOME_GenericFactory_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GNOME_EmbeddableFactory_supports,
   (gpointer) & impl_GNOME_EmbeddableFactory_create_object,
};

/*** vepv structures ***/

static POA_GNOME_View__vepv impl_GNOME_View_vepv = {
   &impl_GNOME_View_base_epv,
   &impl_GNOME_View_GNOME_Unknown_epv,
   &impl_GNOME_View_epv,
};
static POA_GNOME_Embeddable__vepv impl_GNOME_Embeddable_vepv = {
   &impl_GNOME_Embeddable_base_epv,
   &impl_GNOME_Embeddable_GNOME_Unknown_epv,
   &impl_GNOME_Embeddable_epv,
};
static POA_GNOME_EmbeddableFactory__vepv impl_GNOME_EmbeddableFactory_vepv = {
   &impl_GNOME_EmbeddableFactory_base_epv,
   &impl_GNOME_EmbeddableFactory_GNOME_GenericFactory_epv,
   &impl_GNOME_EmbeddableFactory_epv,
};

/*** Stub implementations ***/



static impl_POA_GNOME_View *
impl_GNOME_View__create(PortableServer_POA poa, 
			GNOME_ViewFrame frame, 
			impl_POA_GNOME_Embeddable *embeddable_servant,
			CORBA_Environment * ev)
{

   impl_POA_GNOME_View *newservant;
   PortableServer_ObjectId *objid;
   
   newservant = g_new0(impl_POA_GNOME_View, 1);
   newservant->servant.vepv = &impl_GNOME_View_vepv;
   newservant->poa = poa;
   newservant->frame = CORBA_Object_duplicate (frame, ev);
   /* this is what will be displayed in the component view ! */
   newservant->embeddable_servant = embeddable_servant;
   newservant->widget = gtk_button_new_with_label (embeddable_servant->data);

   POA_GNOME_View__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   CORBA_free(objid);


   return newservant;
}

static void
impl_GNOME_View__destroy(impl_POA_GNOME_View * servant,
			 CORBA_Environment * ev)
{
   PortableServer_ObjectId *objid;

   objid = PortableServer_POA_servant_to_id(servant->poa, servant, ev);
   PortableServer_POA_deactivate_object(servant->poa, objid, ev);
   CORBA_free(objid);

   POA_GNOME_View__fini((PortableServer_Servant) servant, ev);
   g_free(servant);
}

static void
impl_GNOME_View_size_allocate(impl_POA_GNOME_View * servant,
			      CORBA_short width,
			      CORBA_short height, 
			      CORBA_Environment * ev)
{
}

static void
impl_GNOME_View_size_query(impl_POA_GNOME_View * servant,
			   CORBA_short * desired_width,
			   CORBA_short * desired_height,
			   CORBA_Environment * ev)
{
}

static void
impl_GNOME_View_set_window(impl_POA_GNOME_View * servant,
			   GNOME_View_windowid id, 
			   CORBA_Environment * ev)
{
  servant->plug = gtk_plug_new (id);
  gtk_container_add (GTK_CONTAINER(servant->plug), 
		     servant->widget);
  gtk_widget_show_all (servant->plug);
}


void cancel_cb (GtkWidget *widget, 
		gpointer data)
{
  CORBA_Environment ev;
  GtkWidget *window = data;
  impl_POA_GNOME_View *view_servant = (impl_POA_GNOME_View *)
    gtk_object_get_data (GTK_OBJECT (window),
			 "view_servant");

  CORBA_exception_init (&ev);
  gtk_widget_destroy (window);

  GNOME_ViewFrame_view_activated (view_servant->frame,
				  0,
				  &ev);  
  CORBA_exception_free (&ev);
}

void apply_cb (GtkWidget *button, 
	       gpointer data)
{
  GSList *list;
  impl_POA_GNOME_View * servant;
  GtkWidget *label;
  GtkWidget *entry = (GtkWidget *) data;
  GtkWidget *window = gtk_object_get_data (GTK_OBJECT(button),
					   "window");
  impl_POA_GNOME_View *view_servant = (impl_POA_GNOME_View *)
    gtk_object_get_data (GTK_OBJECT (window),
			 "view_servant");
  strcpy (view_servant->embeddable_servant->data, 
	  gtk_entry_get_text (GTK_ENTRY (entry)));

  list = view_servant->embeddable_servant->view_servant_list;
  while (list != g_slist_last (view_servant->embeddable_servant->view_servant_list)) {
    servant = (impl_POA_GNOME_View *)list->data;
    label = GTK_BIN(servant->widget)->child;

    gtk_label_set_text (GTK_LABEL(label),
			view_servant->embeddable_servant->data);
    list = list->next;
  } 
  servant = (impl_POA_GNOME_View *)list->data;
  label = GTK_BIN(servant->widget)->child;
  gtk_label_set_text (GTK_LABEL(label),
		      view_servant->embeddable_servant->data);


}

void ok_cb (GtkWidget *button, 
	    gpointer data)
{
  GtkWidget *entry = (GtkWidget *) data;
  apply_cb (button, entry);
  cancel_cb (button, 
	     gtk_object_get_data (GTK_OBJECT (button), 
				  "window"));
}


static void
impl_GNOME_View_activate(impl_POA_GNOME_View * servant,
			 CORBA_boolean activated, 
			 CORBA_Environment * ev)
{
  static GtkWidget *window = NULL;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *entry;

  if (activated == 1) {

    /* activate view */
    /* sample code to create the external editing 
       window of the view */
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_object_set_data (GTK_OBJECT (window),
			 "view_servant",
			 servant); 
    gtk_signal_connect (GTK_OBJECT (window), 
			"destroy",
			GTK_SIGNAL_FUNC (cancel_cb), 
			window);
           
    gtk_signal_connect (GTK_OBJECT (window),
			"delete_event", 
			GTK_SIGNAL_FUNC (cancel_cb), 
			window);

    vbox = gtk_vbox_new (TRUE, TRUE);
    hbox = gtk_hbox_new (TRUE, TRUE);
    
    entry = gtk_entry_new_with_max_length (10);
    gtk_entry_set_text (GTK_ENTRY (entry), 
			servant->embeddable_servant->data);
    gtk_box_pack_start (GTK_BOX (vbox), 
			entry,
			TRUE, 
			0,
			0);
    
    gtk_box_pack_start (GTK_BOX (vbox), 
			gtk_hseparator_new(), 
			TRUE,
			0,
			0);

    gtk_box_pack_end (GTK_BOX(vbox), hbox,
		      TRUE, 
		      0, 
		      0);
    button = gtk_button_new_with_label ("Cancel");
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(cancel_cb),
			window);
    button = gtk_button_new_with_label ("Apply");
    gtk_object_set_data (GTK_OBJECT (button), 
			 "window",
			 window);
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(apply_cb),
			entry);  
    button = gtk_button_new_with_label ("Ok");
    gtk_object_set_data (GTK_OBJECT (button), 
			 "window",
			 window);
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(ok_cb),
			entry);

    gtk_container_add (GTK_CONTAINER(window), 
		       vbox);
    gtk_widget_show_all (GTK_WIDGET(window));

  } else if (activated == 0 && window != NULL) {
    gtk_widget_destroy (window);
  }
  /* tell the container the view was activated or not*/
  GNOME_ViewFrame_view_activated (servant->frame, 
				  activated, 
				  ev);
}

static void
impl_GNOME_View_reactivate_and_undo(impl_POA_GNOME_View * servant,
				    CORBA_Environment * ev)
{
  /* i am lazy: i don't want to implement this... */
}

static void
impl_GNOME_View_do_verb(impl_POA_GNOME_View * servant,
			CORBA_char * verb_name, 
			CORBA_Environment * ev)
{
  /* i am lazy: i don't want to implement this... */
}

static void
impl_GNOME_View_set_zoom_factor(impl_POA_GNOME_View * servant,
				CORBA_double zoom, 
				CORBA_Environment * ev)
{
  /* i am lazy: i don't want to implement this... */
}

static void
impl_GNOME_View_ref(impl_POA_GNOME_View * servant, 
		    CORBA_Environment * ev)
{
}

static void
impl_GNOME_View_unref(impl_POA_GNOME_View * servant, 
		      CORBA_Environment * ev)
{
}

static CORBA_Object
impl_GNOME_View_query_interface(impl_POA_GNOME_View * servant,
				CORBA_char * repoid, 
				CORBA_Environment * ev)
{
   CORBA_Object retval;

   return retval;
}

static GNOME_Embeddable
impl_GNOME_Embeddable__create(PortableServer_POA poa, 
			      CORBA_Environment * ev)
{
   GNOME_Embeddable retval;
   impl_POA_GNOME_Embeddable *newservant;
   PortableServer_ObjectId *objid;

   newservant = g_new0(impl_POA_GNOME_Embeddable, 1);
   newservant->servant.vepv = &impl_GNOME_Embeddable_vepv;
   newservant->poa = poa;
   strcpy (newservant->data, "Ok ?!");;

   POA_GNOME_Embeddable__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   CORBA_free(objid);
   retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

   return retval;
}

static void
impl_GNOME_Embeddable__destroy(impl_POA_GNOME_Embeddable * servant,
			       CORBA_Environment * ev)
{
   PortableServer_ObjectId *objid;

   objid = PortableServer_POA_servant_to_id(servant->poa, servant, ev);
   PortableServer_POA_deactivate_object(servant->poa, objid, ev);
   CORBA_free(objid);

   POA_GNOME_Embeddable__fini((PortableServer_Servant) servant, ev);
   g_free(servant);
}

static void
impl_GNOME_Embeddable_set_client_site(impl_POA_GNOME_Embeddable * servant,
				      GNOME_ClientSite client_site,
				      CORBA_Environment * ev)
{
  servant->client_site = CORBA_Object_duplicate (client_site, ev);
  /* tell the container that the embeddable will 
     be activated in an external window */
  GNOME_ClientSite_show_window (client_site, 1, ev);
}

static GNOME_ClientSite
impl_GNOME_Embeddable_get_client_site(impl_POA_GNOME_Embeddable * servant,
				      CORBA_Environment * ev)
{
   GNOME_ClientSite retval;

   return retval;
}

static void
impl_GNOME_Embeddable_set_host_name(impl_POA_GNOME_Embeddable * servant,
				    CORBA_char * name,
				    CORBA_char * appname,
				    CORBA_Environment * ev)
{
  servant->host_name = g_strdup (name);

}

static void
impl_GNOME_Embeddable_set_uri(impl_POA_GNOME_Embeddable * servant,
			      CORBA_char * uri, 
			      CORBA_Environment * ev)
{
  servant->uri = g_strdup (uri);

}

static void
impl_GNOME_Embeddable_close(impl_POA_GNOME_Embeddable * servant,
			    GNOME_Embeddable_CloseMode mode,
			    CORBA_Environment * ev)
{
  /* there, i must close the external editing application... */
}

static GNOME_Embeddable_verb_list *
impl_GNOME_Embeddable_get_verb_list(impl_POA_GNOME_Embeddable * servant,
				    CORBA_Environment * ev)
{
   GNOME_Embeddable_verb_list *retval;
   /* our embeddable will not support 
      any non-standard action 
      ie: it won't export any verb
      thus, return an empty verb_list
   */
   
   return retval;
}

static GNOME_View
impl_GNOME_Embeddable_new_view(impl_POA_GNOME_Embeddable * servant,
			       GNOME_ViewFrame frame, 
			       CORBA_Environment * ev)
{
   GNOME_View view;
   impl_POA_GNOME_View *view_servant;
   view_servant = impl_GNOME_View__create (servant->poa, 
					   frame,
					   servant,
					   ev);   
   servant->view_servant_list = g_slist_append (servant->view_servant_list, 
						view_servant);

   view = PortableServer_POA_servant_to_reference(servant->poa, view_servant, ev);

   return view;
}

static void
impl_GNOME_Embeddable_ref(impl_POA_GNOME_Embeddable * servant,
			  CORBA_Environment * ev)
{
}

static void
impl_GNOME_Embeddable_unref(impl_POA_GNOME_Embeddable * servant,
			    CORBA_Environment * ev)
{
}

static CORBA_Object
impl_GNOME_Embeddable_query_interface(impl_POA_GNOME_Embeddable * servant,
				      CORBA_char * repoid,
				      CORBA_Environment * ev)
{
   CORBA_Object retval;

   return retval;
}

static GNOME_EmbeddableFactory
impl_GNOME_EmbeddableFactory__create(PortableServer_POA poa,
				     CORBA_Environment * ev)
{
   GNOME_EmbeddableFactory retval;
   impl_POA_GNOME_EmbeddableFactory *newservant;
   PortableServer_ObjectId *objid;

   newservant = g_new0(impl_POA_GNOME_EmbeddableFactory, 1);
   newservant->servant.vepv = &impl_GNOME_EmbeddableFactory_vepv;
   newservant->poa = poa;
   POA_GNOME_EmbeddableFactory__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   CORBA_free(objid);
   retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

   return retval;
}

static void
impl_GNOME_EmbeddableFactory__destroy(impl_POA_GNOME_EmbeddableFactory *
				      servant, 
				      CORBA_Environment * ev)
{
   PortableServer_ObjectId *objid;

   objid = PortableServer_POA_servant_to_id(servant->poa, servant, ev);
   PortableServer_POA_deactivate_object(servant->poa, objid, ev);
   CORBA_free(objid);

   POA_GNOME_EmbeddableFactory__fini((PortableServer_Servant) servant, ev);
   g_free(servant);
}

static CORBA_boolean
impl_GNOME_EmbeddableFactory_supports(impl_POA_GNOME_EmbeddableFactory *
				      servant, CORBA_char * obj_goad_id,
				      CORBA_Environment * ev)
{
   CORBA_boolean retval;

   return retval;
}

static CORBA_Object
impl_GNOME_EmbeddableFactory_create_object(impl_POA_GNOME_EmbeddableFactory *
					   servant, CORBA_char * goad_id,
					   GNOME_stringlist * params,
					   CORBA_Environment * ev)
{
   CORBA_Object retval = CORBA_OBJECT_NIL;

   retval = impl_GNOME_Embeddable__create (servant->poa, ev);
   /* Use GNOME's default name server */
   goad_server_register (CORBA_OBJECT_NIL, retval, goad_id, "server", ev);
   
   return retval;
}


int main (int argc, char **argv)
{
  CORBA_Environment ev;
  CORBA_ORB orb;
  CORBA_Object root_poa;
  PortableServer_POAManager root_poa_manager;
  CosNaming_NamingContext name_server;
  CORBA_Object obj;

  CORBA_exception_init (&ev);
  orb = gnome_CORBA_init ("a \"simple\" container", 
			  "1.0", 
			  &argc, 
			  argv, 
			  GNORBA_INIT_SERVER_FUNC, 
			  &ev);

  root_poa = CORBA_ORB_resolve_initial_references (orb, 
						   "RootPOA", 
						   &ev);

  root_poa_manager = PortableServer_POA__get_the_POAManager (
		     (PortableServer_POA) root_poa, &ev);
  PortableServer_POAManager_activate (root_poa_manager, &ev);

  /* this function will create all the other 
   * needed interfaces
   */
  obj = impl_GNOME_EmbeddableFactory__create ((PortableServer_POA)
					      root_poa, 
					      &ev);
  /* register against the name service */
  name_server = gnome_name_service_get ();
  goad_server_register (name_server, obj, 
			goad_server_activation_id(), 
			"server", 
			&ev);
  /* finished !! */
  CORBA_exception_free (&ev);
  gtk_main ();
  return 0;
}
