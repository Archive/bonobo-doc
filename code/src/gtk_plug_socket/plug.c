#include "plug.h"
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <ORBitservices/CosNaming.h>

 /*** App-specific servant structures ***/
 typedef struct {
   POA_view_plug servant;
   PortableServer_POA poa;
   GtkWidget *widget;
 } impl_POA_view_plug;

 /*** Implementation stub prototypes ***/
 static void impl_view_plug__destroy(impl_POA_view_plug * servant,
                                     CORBA_Environment * ev);
 static void
  impl_view_plug_set_window_id(impl_POA_view_plug * servant,
 CORBA_unsigned_long winid,
 CORBA_Environment * ev);

 /*** epv structures ***/
 static PortableServer_ServantBase__epv impl_view_plug_base_epv =
 {
    NULL,                        /* _private data */
    NULL,                        /* finalize routine */
    NULL,                        /* default_POA routine */
 };
 static POA_view_plug__epv impl_view_plug_epv =
 {
    NULL,                        /* _private */
    (gpointer) & impl_view_plug_set_window_id,

 };

 /*** vepv structures ***/
 static POA_view_plug__vepv impl_view_plug_vepv =
 {
    &impl_view_plug_base_epv,
    &impl_view_plug_epv,
 };

 /*** Stub implementations ***/
 static view_plug
 impl_view_plug__create(PortableServer_POA poa, 
			CORBA_Environment * ev)
 {
    view_plug retval;
    impl_POA_view_plug *newservant;
    PortableServer_ObjectId *objid;

    newservant = g_new0(impl_POA_view_plug, 1);
    newservant->servant.vepv = &impl_view_plug_vepv;
    newservant->poa = poa;
    newservant->widget = gtk_button_new_with_label ("A small button :)");

    POA_view_plug__init((PortableServer_Servant) newservant, ev);
    objid = PortableServer_POA_activate_object(poa, newservant, ev);
    CORBA_free(objid);
    retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);


    return retval;
 }

 static void
 impl_view_plug__destroy(impl_POA_view_plug * servant, 
			 CORBA_Environment * ev)
 {
    PortableServer_ObjectId *objid;

    objid = PortableServer_POA_servant_to_id(servant->poa, servant, ev);
    PortableServer_POA_deactivate_object(servant->poa, objid, ev);
    CORBA_free(objid);

    POA_view_plug__fini((PortableServer_Servant) servant, ev);
    g_free(servant);
 }

 static void
 impl_view_plug_set_window_id(impl_POA_view_plug * servant,
                              CORBA_unsigned_long winid,
                              CORBA_Environment * ev)
 {
   GtkWidget *window;
   window = gtk_plug_new (winid);
   gtk_container_add (GTK_CONTAINER(window), 
		      servant->widget);
   gtk_widget_show_all (window);
 }

 int main (int argc, char **argv)
 {
   CORBA_Environment ev;
   CORBA_ORB orb;
   PortableServer_POA root_poa;
   PortableServer_POAManager root_poa_manager;
   CosNaming_NamingContext name_server;
   CosNaming_NameComponent name_component[1] = 
   {{"view_plug","server"}};
   CosNaming_Name name = 
   {1, 1, name_component, CORBA_FALSE};

   CORBA_Object obj;

   CORBA_exception_init (&ev);

   orb = gnome_CORBA_init ("a simple socket client", 
                           "0.1", 
                           &argc, 
                           argv, 
                           GNORBA_INIT_SERVER_FUNC, 
                           &ev);
   root_poa = (PortableServer_POA) 
     CORBA_ORB_resolve_initial_references (orb, 
					   "RootPOA", 
					   &ev);
   root_poa_manager = PortableServer_POA__get_the_POAManager (root_poa, 
							      &ev);
   PortableServer_POAManager_activate (root_poa_manager, &ev);

   obj = impl_view_plug__create (root_poa, &ev);

   name_server = gnome_name_service_get ();
   CosNaming_NamingContext_bind (name_server, &name, obj, &ev);

   CORBA_exception_free (&ev);
   gtk_main();
   return 0;

 }
