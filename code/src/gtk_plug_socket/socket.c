#include <orb/orbit.h>
#include <ORBitservices/CosNaming.h>
#include <gnome.h>
#include <gdk/gdkx.h>
#include <libgnorba/gnorba.h>
#include "./plug.h"

 int main (int argc, char **argv)
 {
   CORBA_Environment ev;
   CORBA_ORB orb;
   CosNaming_NamingContext name_server;
   CosNaming_NameComponent name_component[1] = 
   {{"view_plug","server"}};
   CosNaming_Name name = 
   {1, 1, name_component, CORBA_FALSE};
   CORBA_Object obj;
   GtkWidget *win;
   GtkWidget *socket;

   CORBA_exception_init (&ev);
   orb = gnome_CORBA_init ("a simple socket client", 
                           "0.1", 
                           &argc, 
                           argv, 
                           GNORBA_INIT_SERVER_FUNC, 
                           &ev);

   name_server = gnome_name_service_get ();

   obj = CosNaming_NamingContext_resolve (name_server, 
					  &name, 
					  &ev);

   win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   gtk_window_set_default_size (GTK_WINDOW (win), 
				300, 
				300);
   socket = gtk_socket_new ();
   gtk_container_add (GTK_CONTAINER(win), 
		      socket);
   gtk_widget_show_all (win);
   
   view_plug_set_window_id (obj, GDK_WINDOW_XWINDOW(socket->window), &ev);
   CORBA_exception_free (&ev);

   gtk_main ();
   return 0;
 }
