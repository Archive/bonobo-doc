<!-- Maintainer: Diego Sevilla Ruiz <dsevilla@um.es> -->
<!-- <!doctype chapter PUBLIC "-//Davenport//DTD DocBook V3.1//EN" []> -->
     <chapter>
       <title>A hand made containee</title>
      <sect1 id="bonobo-hand-containee-introduction">
	<title>Introduction</title>
        <para>In the previous chapter, we saw the container interfaces responsible
for component embedding. There were three. You have probably noticed how deep their relationship
with the component interfaces was.</para>
        <para>The component has only 2 necessary interfaces. <classname>GNOME::embeddable</classname>
and <classname>GNOME::View</classname> which are closely related to respectively <classname>
GNOME::ClientSite</classname> and <classname>GNOME::ViewFrame</classname>.
A component should of course implement the <classname>GNOME::PersistFile</classname> interface to 
support saving but we'll discuss this later.</para>
      </sect1>
      <sect1 id="bonobo-hand-containee-interfaces">
	<title>The interfaces</title>
         <sect2>
           <title><classname>GNOME::EmbeddableFactory</classname></title>
         <para>The main interface for components is <classname>GNOME::Embeddable</classname>. 
Objects implementing it should be created with a <classname>GNOME::EmbeddableFactory</classname>,
which is a factory for Embeddable objects. Such a factory should be declared with the 
standard .gnorba files we studied in chapter 5.</para>
<programlisting>
module GNOME {

  typedef sequence&lt;string> stringlist;

  interface GenericFactory {
    exception CannotActivate { };
    boolean supports(in string obj_goad_id);

    Object create_object(in string goad_id, in stringlist params)
           raises(CannotActivate);
  };

  interface EmbeddableFactory : GNOME::GenericFactory {
    Embeddable create_path (in string path);
  };
};
</programlisting>
         <itemizedlist>
         <listitem><para>The <function>create_path</function> function is used to handle linking support.
</para></listitem>
         <listitem><para>The <function>create_object</function> function is used to create the object 
itself, as we already discussed it in chapter 5.</para></listitem>
         </itemizedlist>
        </sect2>
        <sect2>
          <title><classname>GNOME::Embeddable</classname></title>
         <para>Here is the <classname>GNOME::embeddable</classname> interface:</para>
<programlisting>
module GNOME {
  interface Embeddable : GNOME::Unknown {

    void set_client_site (in ClientSite client_site);

    ClientSite get_client_site ();

    void set_host_name (in string name, in string appname);

    void set_uri (in string uri);

    exception UserCancelledSave {};
    enum CloseMode {
      SAVE_IF_DIRTY,
      NO_SAVE,
      PROMPT_SAVE
    };
    void close (in CloseMode mode)
          raises (UserCancelledSave);

    struct GnomeVerb {
      string name;
      string label;
      string hint;
    };
    typedef sequence&lt;GnomeVerb> verb_list;
    verb_list get_verb_list ();

    void advise (in AdviseSink advise);

    void unadvise ();

    long get_misc_status (in long type);

    exception MultiViewNotSupported {};
    View new_view (in ViewFrame frame) 
         raises (MultiViewNotSupported);
  };
};
</programlisting>
        <para>This interface is a big, but fortunately easily understandable.</para>
         <itemizedlist>
        <listitem><para>The <function>set_client_site</function> function is used by the container to tell
the component who it is talking to. This function is generally called once the container has created 
the corresponding ClientSite (yes... it's useful) to allow the embeddable created with its factory
to communicate with the container (to handle the size negotiation for example).</para></listitem>

        <listitem><para>The <function>set_host_name</function> function is rather simple: it is used to set
the window's name - displayed in the window handle by the window manager - of the window used
for editing if the object does not support in-place activation. This name should also be used
for the application's window if in-place editing is used.</para></listitem>

        <listitem><para>The <function>close</function> function is the embeddable destructor: it will 
be called by the container when the composed document is closed.</para></listitem>
        <listitem><para>The <function>advise</function> and <function>unadvise</function> functions will be 
discussed later. No real implementation is needed now: the <classname>GNOME::AdviseSink</classname> interface
is not yet defined.</para></listitem>
        <listitem><para><function>new_view</function> is THE main function of this interface. It is 
used by the container to request new views of the component. The component is not obliged to be able 
to give away more than one view of the same object (which is why the MultiViewNotSupported signal 
exists) but is recommended to. This function is the <classname>GNOME::View</classname> factory. One 
should note that
new views are given their <classname>GNOME::ClientSite</classname> upon creation with the "frame" 
parameter.</para></listitem>
        <listitem><para><function>get_misc_status</function> is used for miscellaneous things. Don't really
known what things...</para></listitem>
        <listitem><para><function>get_verb_list</function> allows the container to ask
an Embeddable what special verbs it supports. A verb is a simple non-parametrized action which the
Embeddable can execute through <function>GNOME::View::do_verb</function>. The classic example of a 
verb is "next_page" for a Postscript Viewer.</para></listitem>
       </itemizedlist>
       </sect2>
       <sect2>
         <title><classname>GNOME::View</classname></title>
         <para>Here is this famous <classname>GNOME::View</classname> interface.</para>
<programlisting>
module GNOME {

  interface View : GNOME::Unknown {

    void size_allocate (in short width, in short height);

    void size_query (out short desired_width, out short desired_height);

    typedef unsigned long windowid;
    void set_window (in windowid id);

    void activate (in boolean activated);

    void reactivate_and_undo ();

    void do_verb (in string verb_name);
  
    void set_zoom_factor (in double zoom);
  };
};
</programlisting>
        <itemizedlist>
         <listitem><para><function>size_query</function> and <function>size_allocate</function> are 
used to handle the size negotiation mechanism of the container: when the container needs to be resized, 
it asks all of its children for their preferred size. Then, the container allocates corresponding
areas for these children, given its size,  and notifies them with <function>size_allocate</function>.</para>
<para><function>size_allocate</function> is also used by the container when the child view requests a 
resize with <function>GNOME::ViewFrame::resize_request</function>.</para></listitem>

          <listitem><para><function>set_window</function> is used by the container to give the 
containee a window to draw its view on. The container calls this function and gives the View an 
X window. This is achieved through the use of the GtkSocket/GtkPlug mechanism - already studied 
in chapter 5. see appendix D for sample code.-.</para></listitem>

           <listitem><para><function>activate</function> is used to tell the view to activate 
itself. When called, the view will either create a separate edit window  or will try in-place 
activation, depending on the type of component. Please, note that the Embeddable must tell
the container about its type - in-place or non-in-place - with a call to <function>
GNOME::ClientSite::show</function>.
</para></listitem>

           <listitem><para><function>reactivate_and_undo</function> is called by the container when 
the user did an 'undo' just after having de-activated a component.</para></listitem>

           <listitem><para><function>do_verb</function> is mainly used to ask the component to execute 
simple non-parametrized actions like "next_page" for a Postscript Viewer.
</para></listitem>

           <listitem><para>Finally, the <function>set_zoom_factor</function> function's use is 
obvious: it sets the View zoom factor.</para></listitem>
         </itemizedlist>
       </sect2>
      </sect1>
      <sect1>
	<title>A sample containee</title>
         <para>I have already detailed how this code was written from automatically 
generated code from ORBit. So, here is the raw code.</para>
         <sect2>
           <title>Creating a <classname>GNOME::Embeddable</classname></title>
         <para>The first thing to do is to implement the <classname>GNOME::EmbeddableFactory
</classname>. This is what the following code does. The factory is created, and it is registered 
against the GOAD. Typically, this code will be called by the <function>goad_activate_factory</function> 
function which itself is called by <function>goad_activate_with_id (list, "Component", ...)</function>.</para>
<programlisting>
int main (int argc, char **argv)
{
  CORBA_Environment ev;
  CORBA_ORB orb;
  CORBA_Object root_poa;
  PortableServer_POAManager root_poa_manager;
  CosNaming_NamingContext name_server;
  CORBA_Object obj;

  CORBA_exception_init (&amp;ev);
  orb = gnome_CORBA_init ("a \"simple\" container", 
			  "1.0", 
			  &amp;argc, 
			  argv, 
			  GNORBA_INIT_SERVER_FUNC, 
			  &amp;ev);

  root_poa = CORBA_ORB_resolve_initial_references (orb, 
						   "RootPOA", 
						   &amp;ev);

  root_poa_manager = PortableServer_POA__get_the_POAManager (
		     (PortableServer_POA) root_poa, &amp;ev);
  PortableServer_POAManager_activate (root_poa_manager, &amp;ev);

  /* this function will create all the other 
   * needed interfaces
   */
  obj = impl_GNOME_EmbeddableFactory__create ((PortableServer_POA)
					      root_poa, 
					      &amp;ev);
  /* register against the name service */
  name_server = gnome_name_service_get ();
  goad_server_register (name_server, obj, 
			goad_server_activation_id(), 
			"server", 
			&amp;ev);
  /* finished !! */
  CORBA_exception_free (&amp;ev);
  gtk_main ();
  return 0;
}
</programlisting>
      <para>Once the EmbeddableFactory is created and registered, the GOAD is likely to call the
<function>GNOME::EmbeddableFactory::create_object</function> function to create our Embeddable 
object.</para>
<programlisting>
static CORBA_Object
impl_GNOME_EmbeddableFactory_create_object(impl_POA_GNOME_EmbeddableFactory *
					   servant, CORBA_char * goad_id,
					   GNOME_stringlist * params,
					   CORBA_Environment * ev)
{
   CORBA_Object retval = CORBA_OBJECT_NIL;

   retval = impl_GNOME_Embeddable__create (servant->poa, ev);
   goad_server_register (CORBA_OBJECT_NIL, retval, goad_id, "server", ev);
   
   return retval;
}
</programlisting>
      <para>The <classname>GNOME::Embeddable</classname> creation function, 
<function>impl_GNOME_Embeddable__create</function> is rather simple. It will simply do 
standard stuff and initialize the Embeddable with the "Ok ?!" data. (ie: the data which 
will be displayed on the button).</para>
<programlisting>
static GNOME_Embeddable
impl_GNOME_Embeddable__create(PortableServer_POA poa, 
			      CORBA_Environment * ev)
{
   GNOME_Embeddable retval;
   impl_POA_GNOME_Embeddable *newservant;
   PortableServer_ObjectId *objid;

   newservant = g_new0(impl_POA_GNOME_Embeddable, 1);
   newservant->servant.vepv = &amp;impl_GNOME_Embeddable_vepv;
   newservant->poa = poa;
   strcpy (newservant->data, "Ok ?!");;

   POA_GNOME_Embeddable__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   CORBA_free(objid);
   retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

   return retval;
}
</programlisting>

      <para>The next call which will be done to the Embeddable is to the 
<function>Embeddable::set_client_site</function> function. This function will call the 
<function>ClientSite::show_window</function> function to make sure the ClientSite 
knows that we will be doing some non-in-place activation.</para>
<programlisting>
static void
impl_GNOME_Embeddable_set_client_site(impl_POA_GNOME_Embeddable * servant,
				      GNOME_ClientSite client_site,
				      CORBA_Environment * ev)
{
  servant->client_site = CORBA_Object_duplicate (client_site, ev);
  /* tell the container that the embeddable will 
     be activated in an external window */
  GNOME_ClientSite_show_window (client_site, 1, ev);
}
</programlisting>
        </sect2>
        <sect2>
          <title>Creating a new <classname>GNOME::View</classname></title>
     <para>The next step in this process is creating a new view which will be done with <function>
GNOME::Embeddable::new_view</function>. We'll just create the view itself and store a pointer to this 
view in the embeddable servant. The view is also given a pointer to the corresponding
ViewFrame and Embeddable.</para>
<programlisting>
static GNOME_View
impl_GNOME_Embeddable_new_view(impl_POA_GNOME_Embeddable * servant,
			       GNOME_ViewFrame frame, 
			       CORBA_Environment * ev)
{
   GNOME_View view;
   impl_POA_GNOME_View *view_servant;
   view_servant = impl_GNOME_View__create (servant->poa, 
					   frame,
					   servant,
					   ev);   
   servant->view_servant_list = g_slist_append (servant->view_servant_list, 
						view_servant);

   view = PortableServer_POA_servant_to_reference(servant->poa, view_servant, ev);

   return view;
}
</programlisting>
       <para>The <function>impl_GNOME_View__create</function> function just initializes some stuff.</para>
<programlisting>
static impl_POA_GNOME_View *
impl_GNOME_View__create(PortableServer_POA poa, 
			GNOME_ViewFrame frame, 
			impl_POA_GNOME_Embeddable *embeddable_servant,
			CORBA_Environment * ev)
{

   impl_POA_GNOME_View *newservant;
   PortableServer_ObjectId *objid;
   
   newservant = g_new0(impl_POA_GNOME_View, 1);
   newservant->servant.vepv = &amp;impl_GNOME_View_vepv;
   newservant->poa = poa;
   newservant->frame = CORBA_Object_duplicate (frame, ev);
   newservant->embeddable_servant = embeddable_servant;
   /* this is what will be displayed in the component view ! */
   newservant->widget = gtk_button_new_with_label (embeddable_servant->data);

   POA_GNOME_View__init((PortableServer_Servant) newservant, ev);
   objid = PortableServer_POA_activate_object(poa, newservant, ev);
   CORBA_free(objid);

   return newservant;
}
</programlisting>
        <para>Once the view is created, it will wait to be given an X window. This will be done
by the Container which will call <function>GNOME::View::set_window</function></para>
<programlisting>
static void
impl_GNOME_View_set_window(impl_POA_GNOME_View * servant,
			   GNOME_View_windowid id, 
			   CORBA_Environment * ev)
{
  servant->plug = gtk_plug_new (id);
  gtk_container_add (GTK_CONTAINER(servant->plug), 
		     servant->widget);
  gtk_widget_show_all (servant->plug);
}
</programlisting>
         </sect2>
         <sect2>
           <title>Activating a View</title>
          <para>Views are activated through the <function>GNOME::View::activate</function> function 
which must be called by the container. First, it will check if the View is to be activated or
deactivated and build the editing GUI or not. Last, it will call 
<function>GNOME::ViewFrame::view_activated</function> to tell the container to set on or off the 
cover over the View.</para>
<programlisting>

static void
impl_GNOME_View_activate(impl_POA_GNOME_View * servant,
			 CORBA_boolean activated, 
			 CORBA_Environment * ev)
{
  static GtkWidget *window = NULL;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *entry;

  if (activated == 1) {
    /* activate view */
    /* sample code to create the external editing 
       window of the view */
    /* ...... here, code for the editing GUI ...... */
  } else if (activated == 0 && window != NULL) {
     /* deactivate view */
    gtk_widget_destroy (window);
  }
  /* tell the container the view was activated or not*/
  GNOME_ViewFrame_view_activated (servant->frame, 
				  activated, 
				  ev);
}
</programlisting>
          <para>If the view is to be activated, the GUI will be build:</para>
<programlisting>
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_object_set_data (GTK_OBJECT (window),
			 "view_servant",
			 servant); 
    gtk_signal_connect (GTK_OBJECT (window), 
			"destroy",
			GTK_SIGNAL_FUNC (cancel_cb), 
			window);
           
    gtk_signal_connect (GTK_OBJECT (window),
			"delete_event", 
			GTK_SIGNAL_FUNC (cancel_cb), 
			window);

    vbox = gtk_vbox_new (TRUE, TRUE);
    hbox = gtk_hbox_new (TRUE, TRUE);
    
    entry = gtk_entry_new_with_max_length (10);
    gtk_entry_set_text (GTK_ENTRY (entry), 
			servant->embeddable_servant->data);
    gtk_box_pack_start (GTK_BOX (vbox), 
			entry,
			TRUE, 
			0,
			0);
    
    gtk_box_pack_start (GTK_BOX (vbox), 
			gtk_hseparator_new(), 
			TRUE,
			0,
			0);

    gtk_box_pack_end (GTK_BOX(vbox), hbox,
		      TRUE, 
		      0, 
		      0);
    button = gtk_button_new_with_label ("Cancel");
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(cancel_cb),
			window);
    button = gtk_button_new_with_label ("Apply");
    gtk_object_set_data (GTK_OBJECT (button), 
			 "window",
			 window);
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(apply_cb),
			entry);  
    button = gtk_button_new_with_label ("Ok");
    gtk_object_set_data (GTK_OBJECT (button), 
			 "window",
			 window);
    gtk_box_pack_end (GTK_BOX(hbox), 
		      button,
		      TRUE,
		      0,
		      0);
    gtk_signal_connect (GTK_OBJECT(button), 
			"clicked",
			GTK_SIGNAL_FUNC(ok_cb),
			entry);

    gtk_container_add (GTK_CONTAINER(window), 
		       vbox);
    gtk_widget_show_all (GTK_WIDGET(window));
</programlisting>
        <para>The important idea is that the buttons of the GUI are given enough 
parameters to either close the GUI or apply the changes to all the views of the activated 
Embeddable. Here is for example the <function>apply_cb</function> function which loops through 
all the embeddable views to change their buttons' labels.</para>
<programlisting>
void apply_cb (GtkWidget *button, 
	       gpointer data)
{
  GSList *list;
  impl_POA_GNOME_View * servant;
  GtkWidget *label;
  GtkWidget *entry = (GtkWidget *) data;
  GtkWidget *window = gtk_object_get_data (GTK_OBJECT(button),
					   "window");
  impl_POA_GNOME_View *view_servant = (impl_POA_GNOME_View *)
    gtk_object_get_data (GTK_OBJECT (window),
			 "view_servant");
  strcpy (view_servant->embeddable_servant->data, 
	  gtk_entry_get_text (GTK_ENTRY (entry)));

  list = view_servant->embeddable_servant->view_servant_list;
  while (list != g_slist_last (view_servant->embeddable_servant->view_servant_list)) {
    servant = (impl_POA_GNOME_View *)list->data;
    label = GTK_BIN(servant->widget)->child;

    gtk_label_set_text (GTK_LABEL(label),
			view_servant->embeddable_servant->data);
    list = list->next;
  } 
  servant = (impl_POA_GNOME_View *)list->data;
  label = GTK_BIN(servant->widget)->child;
  gtk_label_set_text (GTK_LABEL(label),
		      view_servant->embeddable_servant->data);
}
</programlisting>
        </sect2>
      </sect1>
     </chapter>