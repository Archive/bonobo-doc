html: files_html directory_html images_html 
files_html: 
	cd sgml && make html && cd ../
directory_html: 
	[ ! -d ./html ] && mkdir html; \
	mv ./sgml/*.html ./html; cp corba.css ./html
images_html: image
	cd image && make html && cd .. && cp ./image/*.png ./html 



ps: images_ps files_ps directory_ps 
images_ps: image
	cd image && make ps && cd .. && cp ./image/*.eps ./sgml
files_ps:
	cd sgml && make ps && cd ../
directory_ps: 
	[ ! -d ./ps ] && mkdir ps; \
	mv ./sgml/*.ps ./sgml/*.dvi ./sgml/*.log \
	./sgml/*.aux ./sgml/*.eps ./ps



sgml: clean
	cd ../ && tar zcvf bonobo-doc.tar.gz bonobo-doc/*

dist: html ps
	tar zcvf bonobo-doc-html.tar.gz html/* && gzip -9 ./ps/corba.ps && \
	mv ./ps/corba.ps.gz bonobo-doc-html.tar.gz ../

clean: 
	cd image && make clean && cd ../ && \
	cd sgml && make clean && cd ../ && \
	cd html && make clean && cd ../ && \
	cd ps && rm -f *.dvi *.log *.aux && cd ../ && \
	rm -f *~ 2>/dev/null 
